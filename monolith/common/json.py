from json import JSONEncoder
from django.db.models import QuerySet
from datetime import datetime


class DateEncoder(JSONEncoder):
    def default(self, o):
        # if o is an instance of datetime
        if isinstance(o, datetime):
        #    return o.isoformat()
            return o.isoformat()
        # otherwise
        else:
        #    return super().default(o)
            return super().default(o)

class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)

class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    # after adding the location
    encoders = {}

    def default(self, o):
        #   if the object to decode is the same class as what's in the
        #   model property, then
        if isinstance(o, self.model):
        #     * create an empty dictionary that will hold the property names
        #       as keys and the property values as values
            d={}
        # when adding a list view
            if hasattr(o, "get_api_url"):
                d["href"] = o.get_api_url()
        #     * for each name in the properties list
            for property in self.properties:
        #         * get the value of that property from the model instance
        #           given just the property name
                value = getattr(o, property)
        # after adding the location
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
        #         * put it into the dictionary with that property name as
        #           the key
                d[property] = value
        # after adding location
            d.update(self.get_extra_data(o))
        #     * return the dictionary
            return d
        #   otherwise,
        #       return super().default(o)  # From the documentation
        else:
            return super().default(o)
    # after adding location add extra data for nearly everything now
    def get_extra_data(self, o):
        return {}
